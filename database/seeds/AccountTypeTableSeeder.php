<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AccountTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = DB::table('account_types')->get();
        if (count($types) == 0) {
            DB::table('account_types')->insert([
                    'id' => 1,
                    'name' => 'Débito',
            ]);
            DB::table('account_types')->insert([
                    'id' => 2,
                    'name' => 'Crédito',
            ]);
        }
    }
}
