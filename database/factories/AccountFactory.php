<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\Account;
use App\Models\AccountType;
use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Account::class, function (Faker $faker) {
    return [
        'number' => $faker->numerify('########'),
        'type' => AccountType::$DEBIT,
        'amount' => 0.0,
        'credit' => 0.0,
    ];
});
