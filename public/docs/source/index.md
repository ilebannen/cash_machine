---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)

<!-- END_INFO -->

#Manejo de Cuentas

Class Controller
<!-- START_ae66d1ba0ed0531684cc35e67177f9fa -->
## api/v1/account/{id}
> Example request:

```bash
curl -X GET -G "http://localhost/api/v1/account/1" 
```
```javascript
const url = new URL("http://localhost/api/v1/account/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (404):

```json
{
    "error": "Model not found"
}
```

### HTTP Request
`GET api/v1/account/{id}`


<!-- END_ae66d1ba0ed0531684cc35e67177f9fa -->

<!-- START_700b6083aece5829c3fc1304c926b8c6 -->
## api/v1/account
> Example request:

```bash
curl -X POST "http://localhost/api/v1/account" 
```
```javascript
const url = new URL("http://localhost/api/v1/account");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST api/v1/account`


<!-- END_700b6083aece5829c3fc1304c926b8c6 -->

<!-- START_e10a14c2824e006df2897065cdb84d50 -->
## api/v1/account/{id}/withdraw
> Example request:

```bash
curl -X PUT "http://localhost/api/v1/account/1/withdraw" 
```
```javascript
const url = new URL("http://localhost/api/v1/account/1/withdraw");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`PUT api/v1/account/{id}/withdraw`


<!-- END_e10a14c2824e006df2897065cdb84d50 -->

<!-- START_e336c0107927d9818dda0e5fcb0c1438 -->
## api/v1/account/{id}/deposit
> Example request:

```bash
curl -X PUT "http://localhost/api/v1/account/1/deposit" 
```
```javascript
const url = new URL("http://localhost/api/v1/account/1/deposit");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`PUT api/v1/account/{id}/deposit`


<!-- END_e336c0107927d9818dda0e5fcb0c1438 -->

#Manejo de Usuarios

Class Controller
<!-- START_d7f5c16f3f30bc08c462dbfe4b62c6b9 -->
## api/v1/user
> Example request:

```bash
curl -X GET -G "http://localhost/api/v1/user" 
```
```javascript
const url = new URL("http://localhost/api/v1/user");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": []
}
```

### HTTP Request
`GET api/v1/user`


<!-- END_d7f5c16f3f30bc08c462dbfe4b62c6b9 -->

<!-- START_dd3d5501615121fcb8ef0f5ced2a1ecd -->
## api/v1/user/{id}
> Example request:

```bash
curl -X GET -G "http://localhost/api/v1/user/1" 
```
```javascript
const url = new URL("http://localhost/api/v1/user/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (404):

```json
{
    "error": "Model not found"
}
```

### HTTP Request
`GET api/v1/user/{id}`


<!-- END_dd3d5501615121fcb8ef0f5ced2a1ecd -->

<!-- START_96b8840d06e94c53a87e83e9edfb44eb -->
## api/v1/user
> Example request:

```bash
curl -X POST "http://localhost/api/v1/user" 
```
```javascript
const url = new URL("http://localhost/api/v1/user");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST api/v1/user`


<!-- END_96b8840d06e94c53a87e83e9edfb44eb -->

<!-- START_ad3b44e69f2f97d33193276f45379b9f -->
## api/v1/user/{id}
> Example request:

```bash
curl -X PUT "http://localhost/api/v1/user/1" 
```
```javascript
const url = new URL("http://localhost/api/v1/user/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`PUT api/v1/user/{id}`


<!-- END_ad3b44e69f2f97d33193276f45379b9f -->

<!-- START_f23e23e5a9b1d819cf366191ee8973b7 -->
## api/v1/user/{id}
> Example request:

```bash
curl -X DELETE "http://localhost/api/v1/user/1" 
```
```javascript
const url = new URL("http://localhost/api/v1/user/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`DELETE api/v1/user/{id}`


<!-- END_f23e23e5a9b1d819cf366191ee8973b7 -->

<!-- START_dcfd3249a70a98567ef8a6f90ca44a92 -->
## api/v1/user/{id}/accounts
> Example request:

```bash
curl -X GET -G "http://localhost/api/v1/user/1/accounts" 
```
```javascript
const url = new URL("http://localhost/api/v1/user/1/accounts");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (404):

```json
{
    "error": "Model not found"
}
```

### HTTP Request
`GET api/v1/user/{id}/accounts`


<!-- END_dcfd3249a70a98567ef8a6f90ca44a92 -->


