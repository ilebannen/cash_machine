## ClickBus Backend Test Implementation Laravel

Se ha creado la implementación del Test con el framework Laravel 5.8

### Descargar el proyecto 
Repositorio: https://bitbucket.org/ilebannen/cash_machine/  
git clone https://bitbucket.org/ilebannen/cash_machine.git
cd cash_machine

### Instalar dependencias
composer install

### Crear DB
Crear la base de datos en MySQL o MariaDB  
DB: cash_db

### Configurar entorno
Crear el archivo .env (basado de .env.example) y configurar parámetros de Base de Datos
DB_HOST=127.0.0.1  
DB_PORT=3306  
DB_DATABASE=cash_db  
DB_USERNAME=root  
DB_PASSWORD=root_password

### Cargar Tablas en la DB
php artisan config:cache
php artisan migrate:install --seed

### Ejecutar pruebas
./vendor/bin/phpunit --verbose

### Ver Documentacion de API
Ver en un navegador la documentación de API en http://localhost/docs


### Docker Compose
Para ejecutar en Docker:  
chown -R 1000.1000 ./*
docker-compose up -d

docker-compose exec app cp .env.dockerfile .env  
docker-compose exec app cp .env.dockerfile .env.testing  
docker-compose exec app php artisan config:cache  
docker-compose exec app php artisan migrate --seed

Ir al navegador http://localhost:8000

Para ejecutar las pruebas:  
docker-compose exec app ./vendor/bin/phpunit --verbose