<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Http\Resources\AccountResource;
use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

/**
 * @group Manejo de Usuarios
 *
 * Class Controller
 * @package App\Http\Controllers
 */
class UserController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getUsers()
    {
        return UserResource::collection(User::all());
    }

    /**
     * @param Request $request
     * @param $id
     * @return UserResource
     */
    public function getUser(Request $request, $id)
    {
        return new UserResource(User::findOrFail($id));
    }

    /**
     * @param UserRequest $request
     * @return mixed
     */
    public function createUser(UserRequest $request)
    {
        return User::create($request->validated());
    }

    /**
     * @param Request $request
     * @param $id
     * @return UserResource
     */
    public function updateUser(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->update($request->all());
        return new UserResource($user);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function deleteUser(Request $request, $id)
    {
        User::find($id)->delete();
        return Response::make("", 204);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getAccountsUser(Request $request, $id) {
        $user = User::with(['accounts'])->findOrFail($id);
        return AccountResource::collection($user->accounts);
    }
}
