<?php

namespace App\Http\Controllers;

use App\Exceptions\OutOfMoneyException;
use App\Http\Requests\AccountRequest;
use App\Http\Requests\AmountRequest;
use App\Http\Resources\AccountResource;
use App\Lib\Accounts\FactoryAccounts;
use App\Models\Account;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

/**
 * @group Manejo de Cuentas
 *
 * Class Controller
 * @package App\Http\Controllers
 */
class AccountController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @param Request $request
     * @param $id
     * @return AccountResource
     */
    public function getAccount(Request $request, $id)
    {
        $account = Account::findOrFail($id);
        return new AccountResource($account);
    }

    /**
     * @param AccountRequest $request
     * @return mixed
     */
    public function createAccount(AccountRequest $request)
    {
        return Account::create($request->validated());
    }

    /**
     * @param AmountRequest $request
     * @param $id
     * @return AccountResource|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function withdraw(AmountRequest $request, $id)
    {
        $account = Account::findOrFail($id);
        $accountOperation = FactoryAccounts::getAccountOperation($account);
        try {
            $accountOperation->withdraw($request->get('amount'));
        } catch (OutOfMoneyException $e) {
            \Log::error($e);
            return Response::make("", 304);
        } catch (\App\Exceptions\OutOfCreditException $e) {
            \Log::error($e);
            return Response::make("", 304);
        }
        return new AccountResource($accountOperation->getModel());
    }

    /**
     * @param AmountRequest $request
     * @param $id
     * @return AccountResource
     * @throws \Exception
     */
    public function deposit(AmountRequest $request, $id)
    {
        $account = Account::findOrFail($id);
        $accountOperation = FactoryAccounts::getAccountOperation($account);
        $accountOperation->deposit($request->get('amount'));
        return new AccountResource($accountOperation->getModel());
    }
}
