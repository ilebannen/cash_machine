<?php


namespace App\Lib\Accounts;

use App\Models\Account;

/**
 * Class AbstractFactoryAccounts
 *
 * @package App\Lib\Accounts
 */
abstract class AbstractFactoryAccounts
{
    /**
     *
     * @param Account $account
     * @return mixed
     */
    abstract static function getAccountOperation(Account $account);

}