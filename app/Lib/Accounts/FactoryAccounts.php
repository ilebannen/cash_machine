<?php


namespace App\Lib\Accounts;

use App\Models\Account;
use App\Models\AccountType;

/**
 * Class FactoryAccounts
 *
 * @package App\Lib\Accounts
 *
 */
class FactoryAccounts extends AbstractFactoryAccounts
{
    /**
     *
     * @param $account_number
     */
    public static function getAccountOperation(Account $account)
    {
        if ($account && $account->id && $account->type) {
            switch ($account->type) {
                case AccountType::$DEBIT:
                    return new DebitAccount($account);

                case AccountType::$CREDIT:
                    return new CreditAccount($account);
            }
            throw new \Exception("Tipo de cuenta invalido");
        } else {
            throw new \Exception("La cuenta solicitada no existe");
        }
    }

}
