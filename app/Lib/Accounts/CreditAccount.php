<?php


namespace App\Lib\Accounts;

use App\Exceptions\OutOfCreditException;
use App\Models\Account;

/**
 * Class CreditAccount
 *
 * @package App\Lib\Accounts
 */
class CreditAccount implements AccountOperationContract
{
    protected $model;

    /**
     * CreditAccount constructor.
     */
    public function __construct(Account $account)
    {
        $this->model = $account;
    }

    /**
     * @param $amount
     */
    public function withdraw($amount)
    {
        $cost = $amount * .1;
        $total_amount = $amount + $cost;
        \DB::transaction(
            function () use ($total_amount) {
                $account = Account::lockForUpdate()->find($this->model->id);
                if ($account->amount >= $total_amount) {
                    $account->amount -= $total_amount;
                    $account->save();
                } else {
                    throw new OutOfCreditException("No tiene suficiente crédito en su cuenta ".$account->amount.' wants '.$total_amount);
                }
            }
        );
    }

    /**
     * @param $amount
     * @return mixed|void
     */
    public function deposit($amount)
    {
        \DB::transaction(
            function () use ($amount) {
                $account = Account::lockForUpdate()->find($this->model->id);
                $account->amount += $amount;
                $account->save();
            }
        );
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }
}
