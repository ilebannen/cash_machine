<?php


namespace App\Lib\Accounts;


interface AccountOperationContract
{
    /**
     * @param $amount
     * @return mixed
     */
    public function withdraw($amount);

    /**
     * @param $amount
     * @return mixed
     */
    public function deposit($amount);

    /**
     * @param $amount
     * @return mixed
     */
    public function getModel();
}