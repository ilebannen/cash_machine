<?php


namespace App\Lib\Accounts;


use App\Exceptions\OutOfMoneyException;
use App\Models\Account;

class DebitAccount implements AccountOperationContract
{

    protected $model;

    /**
     * DebitAccount constructor.
     */
    public function __construct(Account $account)
    {
        $this->model = $account;
    }

    /**
     * @param $amount
     * @return mixed|void
     */
    public function withdraw($amount)
    {
        \DB::transaction(
            function () use ($amount) {
                $account = Account::lockForUpdate()->find($this->model->id);
                if ($account->amount < $amount) {
                    throw new OutOfMoneyException("No hay sufienciente fondos. Max. ".$account->amount.' wants '.$amount);
                } else {
                    $account->amount -= $amount;
                    $account->save();
                }
            }
        );
    }

    /**
     * @param $amount
     * @return mixed|void
     */
    public function deposit($amount)
    {
        \DB::transaction(
            function () use ($amount) {
                $account = Account::lockForUpdate()->find($this->model->id);
                $account->amount += $amount;
                $account->save();
            }
        );
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }
}
