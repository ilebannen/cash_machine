<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccountType extends Model
{
    public static $DEBIT = 1;
    public static $CREDIT = 2;
}
