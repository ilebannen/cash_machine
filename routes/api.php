<?php

use App\Exceptions\OutOfMoneyException;
use App\Http\Requests\AccountRequest;
use App\Http\Requests\AmountRequest;
use App\Http\Requests\UserRequest;
use App\Http\Requests\UserResquest;
use App\Http\Resources\AccountResource;
use App\Http\Resources\UserResource;
use App\Lib\Accounts\FactoryAccounts;
use App\Models\Account;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::prefix('v1')->group(function () {

    Route::get('/user', 'UserController@getUsers')->name('user_list');
    Route::get('/user/{id}', 'UserController@getUser')->name('user_get');
    Route::post('/user', 'UserController@createUser')->name('user_create');
    Route::put('/user/{id}', 'UserController@updateUser')->name('user_update');
    Route::delete('/user/{id}', 'UserController@deleteUser')->name('user_delete');
    Route::get('/user/{id}/accounts', 'UserController@getAccountsUser')->name('user_get_accounts');

    Route::get('/account/{id}', 'AccountController@getAccount')->name('account_get');
    Route::post('/account', 'AccountController@createAccount')->name('account_create');
    Route::put('/account/{id}/withdraw', 'AccountController@withdraw')->name('account_withdraw');
    Route::put('/account/{id}/deposit', 'AccountController@deposit')->name('account_deposit');

    Route::fallback(function () {
        return response()->json(['message' => 'Page Not Found'], 404);
    });
});