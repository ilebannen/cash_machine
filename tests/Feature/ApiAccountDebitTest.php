<?php

namespace Tests\Feature;

use App\Lib\Accounts\AccountOperationContract;
use App\Lib\Accounts\FactoryAccounts;
use App\Models\Account;
use App\Models\AccountType;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ApiAccountDebitTest extends TestCase
{
    /**
     *
     */
    public function testQueryAccount()
    {
        $user = factory(User::class)->make();
        $user->save();
        $account = factory(Account::class)->make();
        $account->user_id = $user->id;
        $account->save();

        $response = $this->json('GET', '/api/v1/account/'.$account->id);
        $response->assertStatus(200);
    }

    public function testCreateAccount()
    {
        $this->seed('AccountTypeTableSeeder');
        $user = factory(User::class)->make();
        $user->save();
        $response = $this->json('POST', '/api/v1/account/',
            [
                'number' => '88798799',
                'type' => AccountType::$DEBIT,
                'amount' => 5000,
                'user_id' => $user->id
            ]
        );
        $response->assertStatus(201);

        $account_id = $response->json('id');
        $response = $this->json('GET', '/api/v1/account/'.$account_id);
        $response->assertStatus(200);
        $response->assertJson(['data' => ['credit' => null]]);
    }

    public function testCreateAccountValidations()
    {
        $user = factory(User::class)->make();
        $user->save();
        $response = $this->json('POST', '/api/v1/account/',
            [
                'number' => '88787991',
                'type' => AccountType::$DEBIT,
                'amount' => -10,
                'user_id' => $user->id
            ]
        );
        $response->assertStatus(422);
        $response = $this->json('POST', '/api/v1/account/',
            [
                'number' => '88787991',
                'type' => 20,
                'amount' => 5000,
                'user_id' => $user->id
            ]
        );
        $response->assertStatus(422);

        $response = $this->json('POST', '/api/v1/account/',
            [
                'number' => '88787991',
                'type' => AccountType::$DEBIT,
                'amount' => 5000,
                'user_id' => 456
            ]
        );
        $response->assertStatus(422);

        $response = $this->json('POST', '/api/v1/account/',
            [
                'number' => '8878799',
                'type' => AccountType::$DEBIT,
                'amount' => 5000,
                'user_id' => $user->id
            ]
        );
        $response->assertStatus(422);
    }

    public function testAccountOperationValidation()
    {
        $user = factory(User::class)->make();
        $user->save();
        $account = factory(Account::class)->make();
        $account->user_id = $user->id;
        $account->save();

        $response = $this->json('PUT', '/api/v1/account/'.$account->id.'/deposit', ['amount' => 'sadasd']);
        $response->assertStatus(422);

        $response = $this->json('PUT', '/api/v1/account/'.$account->id.'/deposit', ['amount' => -50]);
        $response->assertStatus(422);

        $response = $this->json('PUT', '/api/v1/account/'.$account->id.'/withdraw', ['amount' => 'sadsd']);
        $response->assertStatus(422);

        $response = $this->json('PUT', '/api/v1/account/'.$account->id.'/withdraw', ['amount' => -2000]);
        $response->assertStatus(422);

    }

    public function testAccountDeposit()
    {
        $user = factory(User::class)->make();
        $user->save();
        $account = factory(Account::class)->make();
        $account->user_id = $user->id;
        $account->save();

        $response = $this->json('PUT', '/api/v1/account/'.$account->id.'/deposit', ['amount' => 1500]);
        $response->assertStatus(200);
        $response = $this->json('GET', '/api/v1/account/'.$account->id);
        $response->assertStatus(200);
        $response->assertJson(['data' => ['amount' => 1500.00]]);
        $response = $this->json('PUT', '/api/v1/account/'.$account->id.'/deposit', ['amount' => 500]);
        $response->assertStatus(200);
        $response = $this->json('GET', '/api/v1/account/'.$account->id);
        $response->assertStatus(200);
        $response->assertJson(['data' => ['amount' => 2000.00]]);
    }

    public function testAccountWithdraw()
    {
        $user = factory(User::class)->make();
        $user->save();
        $account = factory(Account::class)->make();
        $account->user_id = $user->id;
        $account->save();

        $response = $this->json('PUT', '/api/v1/account/'.$account->id.'/deposit', ['amount' => 1500]);
        $response->assertStatus(200);
        $response = $this->json('GET', '/api/v1/account/'.$account->id);
        $response->assertStatus(200);
        $response->assertJson(['data' => ['amount' => 1500.00]]);

        $response = $this->json('PUT', '/api/v1/account/'.$account->id.'/withdraw', ['amount' => 500]);
        $response->assertStatus(200);
        $response = $this->json('GET', '/api/v1/account/'.$account->id);
        $response->assertStatus(200);
        $response->assertJson(['data' => ['amount' => 1000.00]]);

        $response = $this->json('PUT', '/api/v1/account/'.$account->id.'/withdraw', ['amount' => 300]);
        $response->assertStatus(200);
        $response = $this->json('GET', '/api/v1/account/'.$account->id);
        $response->assertStatus(200);
        $response->assertJson(['data' => ['amount' => 700.00]]);
    }

    public function testAccountWithdrawOutOfMoneyException()
    {
        $user = factory(User::class)->make();
        $user->save();
        $account = factory(Account::class)->make();
        $account->user_id = $user->id;
        $account->save();

        $response = $this->json('PUT', '/api/v1/account/'.$account->id.'/deposit', ['amount' => 1500]);
        $response->assertStatus(200);
        $response = $this->json('GET', '/api/v1/account/'.$account->id);
        $response->assertStatus(200);
        $response->assertJson(['data' => ['amount' => 1500.00]]);

        $response = $this->json('PUT', '/api/v1/account/'.$account->id.'/withdraw', ['amount' => 1501]);
        $response->assertStatus(304);
        $response = $this->json('GET', '/api/v1/account/'.$account->id);
        $response->assertStatus(200);
        $response->assertJson(['data' => ['amount' => 1500.00]]);
    }
}
