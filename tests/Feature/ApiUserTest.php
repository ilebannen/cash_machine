<?php

namespace Tests\Feature;

use App\Models\Account;
use App\User;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ApiUserTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCreateUser()
    {
        $response = $this->json('POST', '/api/v1/user',
            [
                'name' => 'Edo',
                'email' => 'algo@gmail.com',
                'password' => Hash::make('algo')
            ]);
        $response->assertStatus(201);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCreateUserValidation()
    {
        $response = $this->json('POST', '/api/v1/user',
            [
                'name' => 'Edo',
            ]);
        $response->assertStatus(422);
    }

    public function testQueryUsers()
    {
        $response = $this->json('GET', '/api/v1/user');
        $response->assertStatus(200);
    }

    public function testQueryUser()
    {
        $user = factory(User::class)->make();
        $user->save();
        $response2 = $this->json('GET', '/api/v1/user/'.$user->id);
        $response2->assertStatus(200);
        $response2->assertJson(['data' => $user->toArray()]);
    }

    public function testUpdateUser()
    {
        $user = factory(User::class)->make();
        $user->save();
        $response2 = $this->json('PUT', '/api/v1/user/'.$user->id, ['name' => 'Alf']);
        $response2->assertStatus(200);

        $user = User::find($user->id);
        $this->assertEquals('Alf', $user->name);
    }

    public function testDeleteUser()
    {
        $user = factory(User::class)->make();
        $user->save();
        $response2 = $this->json('DELETE', '/api/v1/user/'.$user->id);
        $response2->assertStatus(204);

        $user = User::find($user->id);
        $this->assertNull($user);
    }

}
