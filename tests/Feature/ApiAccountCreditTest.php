<?php

namespace Tests\Feature;

use App\Lib\Accounts\AccountOperationContract;
use App\Lib\Accounts\FactoryAccounts;
use App\Models\Account;
use App\Models\AccountType;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ApiAccountCreditTest extends TestCase
{
    /**
     *
     */
    public function testQueryAccount()
    {
        $user = factory(User::class)->make();
        $user->save();
        $account = factory(Account::class)->make();
        $account->type = AccountType::$CREDIT;
        $account->amount = 5000;
        $account->user_id = $user->id;
        $account->save();

        $user = factory(User::class)->make();
        $user->save();


        $response = $this->json('GET', '/api/v1/account/'.$account->id);
        $response->assertStatus(200);
    }

    public function testCreateAccount()
    {
        $this->seed('AccountTypeTableSeeder');
        $user = factory(User::class)->make();
        $user->save();
        $response = $this->json('POST', '/api/v1/account/',
            [
                'number' => '88798799',
                'type' => AccountType::$CREDIT,
                'amount' => 5000,
                'user_id' => $user->id
            ]
        );
        $response->assertStatus(201);

        $account_id = $response->json('id');
        $response = $this->json('GET', '/api/v1/account/'.$account_id);
        $response->assertStatus(200);
        $response->assertJson(['data' => ['credit' => 5000]]);
    }

    public function testCreateAccountValidations()
    {
        $user = factory(User::class)->make();
        $user->save();
        $response = $this->json('POST', '/api/v1/account/',
            [
                'number' => '88787991',
                'type' => AccountType::$CREDIT,
                'amount' => -10,
                'user_id' => $user->id
            ]
        );
        $response->assertStatus(422);
        $response = $this->json('POST', '/api/v1/account/',
            [
                'number' => '88787991',
                'type' => 20,
                'amount' => 5000,
                'user_id' => $user->id
            ]
        );
        $response->assertStatus(422);

        $response = $this->json('POST', '/api/v1/account/',
            [
                'number' => '88787991',
                'type' => AccountType::$CREDIT,
                'amount' => 5000,
                'user_id' => 456
            ]
        );
        $response->assertStatus(422);

        $response = $this->json('POST', '/api/v1/account/',
            [
                'number' => '8878799',
                'type' => AccountType::$CREDIT,
                'amount' => 5000,
                'user_id' => $user->id
            ]
        );
        $response->assertStatus(422);
    }

    public function testAccountOperationValidation()
    {
        $user = factory(User::class)->make();
        $user->save();
        $account = factory(Account::class)->make();
        $account->type = AccountType::$CREDIT;
        $account->amount = 5000;
        $account->user_id = $user->id;
        $account->save();

        $response = $this->json('PUT', '/api/v1/account/' . $account->id . '/withdraw', ['amount' => 'asd']);
        $response->assertStatus(422);

        $response = $this->json('PUT', '/api/v1/account/' . $account->id . '/withdraw', ['amount' => -10]);
        $response->assertStatus(422);

        $response = $this->json('PUT', '/api/v1/account/'.$account->id.'/deposit', ['amount' => 'asd']);
        $response->assertStatus(422);

        $response = $this->json('PUT', '/api/v1/account/'.$account->id.'/deposit', ['amount' => -500]);
        $response->assertStatus(422);
    }


    public function testAccountDeposit()
    {
        $user = factory(User::class)->make();
        $user->save();
        $account = factory(Account::class)->make();
        $account->type = AccountType::$CREDIT;
        $account->amount = 5000;
        $account->user_id = $user->id;
        $account->save();

        $response = $this->json('PUT', '/api/v1/account/'.$account->id.'/withdraw', ['amount' => 1000]);
        $response->assertStatus(200);
        $response = $this->json('GET', '/api/v1/account/'.$account->id);
        $response->assertStatus(200);
        $response->assertJson(['data' => ['amount' => 3900.00]]);


        $response = $this->json('PUT', '/api/v1/account/'.$account->id.'/withdraw', ['amount' => 500]);
        $response->assertStatus(200);
        $response = $this->json('GET', '/api/v1/account/'.$account->id);
        $response->assertStatus(200);
        $response->assertJson(['data' => ['amount' => 3350.00]]);

        $response = $this->json('PUT', '/api/v1/account/'.$account->id.'/deposit', ['amount' => 1000]);
        $response->assertStatus(200);
        $response = $this->json('GET', '/api/v1/account/'.$account->id);
        $response->assertStatus(200);
        $response->assertJson(['data' => ['amount' => 4350.00]]);

        $response = $this->json('PUT', '/api/v1/account/'.$account->id.'/withdraw', ['amount' => 2500]);
        $response->assertStatus(200);
        $response = $this->json('GET', '/api/v1/account/'.$account->id);
        $response->assertStatus(200);
        $response->assertJson(['data' => ['amount' => 1600.00]]);

        $response = $this->json('PUT', '/api/v1/account/'.$account->id.'/deposit', ['amount' => 3400]);
        $response->assertStatus(200);
        $response = $this->json('GET', '/api/v1/account/'.$account->id);
        $response->assertStatus(200);
        $response->assertJson(['data' => ['amount' => 5000.00]]);
    }

    public function testAccountWithdraw()
    {
        $user = factory(User::class)->make();
        $user->save();
        $account = factory(Account::class)->make();
        $account->type = AccountType::$CREDIT;
        $account->amount = 5000;
        $account->user_id = $user->id;
        $account->save();

        $response = $this->json('PUT', '/api/v1/account/'.$account->id.'/withdraw', ['amount' => 500]);
        $response->assertStatus(200);
        $response = $this->json('GET', '/api/v1/account/'.$account->id);
        $response->assertStatus(200);
        $response->assertJson(['data' => ['amount' => 4450.00]]);

        $response = $this->json('PUT', '/api/v1/account/'.$account->id.'/withdraw', ['amount' => 3000]);
        $response->assertStatus(200);
        $response = $this->json('GET', '/api/v1/account/'.$account->id);
        $response->assertStatus(200);
        $response->assertJson(['data' => ['amount' => 1150.00]]);
    }

    public function testAccountWithdrawOutOfCreditException()
    {
        $user = factory(User::class)->make();
        $user->save();
        $account = factory(Account::class)->make();
        $account->type = AccountType::$CREDIT;
        $account->amount = 5000;
        $account->user_id = $user->id;
        $account->save();

        $response = $this->json('PUT', '/api/v1/account/'.$account->id.'/withdraw', ['amount' => 5000]);
        $response->assertStatus(304);
        $response = $this->json('GET', '/api/v1/account/'.$account->id);
        $response->assertStatus(200);
        $response->assertJson(['data' => ['amount' => 5000.00]]);
    }
}
